# ISPConfig plugin

## Application informations 
This application is an Add-ons for ISPConfig used for manage domain destination

## Requirements
- ISPConfig 3.1+
  - php-curl, php-xml, php-xmlrpc
- Barracuda spam firewall (also know as Email Security Gateway)
- Firewall permission

## Features 
When you add, edit or remove a mail domain from ISPConfig the plugin made:
- Add mail domain into BSF
- Set destination server
- Remove domain from BSF
- Possibility to exclude domain, prevent involunary change

## How To implement this module
- Create BSF API user
- Deploy this plugin into -> /usr/local/ispconfig/interface/lib/plugins
- Configure the plugin with your BSF api settings -> /usr/local/ispconfig/interface/lib/config.inc.local.php
  - $conf["bsf_hostname"] 	= 'Barracuda hostname';
  - $conf["bsf_password"] 	= 'Api password';
  - $conf["bsf_excluded"]	= ['domain1.com', 'domain2.com', '...']; // If not used, create an empty array
  - $conf["bsf_debug"]		= TRUE / FALSE;
 
## ToDo
- Move the XML library outside the plugin
- Barracuda error return treatment

# License
Copyright (c) 2021, Oricom Internet
All rights reserved.
